<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PrincipalController extends Controller
{
    protected $urlLocal = 'http://localhost/stilet/';

    public function index(){
        // if(!Session::has('api_token')){
        //     $this->getToken();
        // }
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        $response = $http->get(env('API_URL',$this->urlLocal).'api/bannersPrincipales');
        $bannersPrincipales = json_decode((string) $response->getBody(), true);

        $response = $http->get(env('API_URL',$this->urlLocal).'api/marcasPrincipales');
        $marcasPrincipales = json_decode((string) $response->getBody(), true);

        $response = $http->get(env('API_URL',$this->urlLocal).'api/nuevasColecciones');
        $nuevasColecciones = json_decode((string) $response->getBody(), true);

        $response = $http->get(env('API_URL',$this->urlLocal).'api/promociones');
        $promociones = json_decode((string) $response->getBody(), true);

        $response = $http->get(env('API_URL',$this->urlLocal).'api/ultimosPares');
        $ultimosPares = json_decode((string) $response->getBody(), true);

        return view('principal.index',compact('categorias',
            'bannersPrincipales','marcasPrincipales','nuevasColecciones','promociones','ultimosPares'));
    }

    public function productos(Request $request){
        if(!$request->has('idf'))
            return redirect('/');
        else
            Session::put('idf',$request->idf);

        if($request->has('idc'))
            Session::put('idc',$request->idc);

        if($request->has('idt')){
            Session::put('idt',$request->idt);
        }else{
            Session::put('idt',0);
        }

        if($request->has('page'))
            Session::put('page',$request->page);
        else
            Session::put('page',1);


        $http = new Client();

        //Categorias HEader
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        //Productos View
        $response2 = $http->get(env('API_URL',$this->urlLocal).'api/productosFirst', [
            'query' => [
                'idf' => Session::get('idf',0),
                'idc' => Session::get('idc',0),
                'idt' => Session::get('idt',0),
                'page' => Session::get('page',1)
                ]
        ]);
        $productos = json_decode((string) $response2->getBody(), true);

        return view('principal.list_products',compact('categorias','productos'));
    }

    public function promociones(Request $request){
        Session::put('idf',0);

        if($request->has('idc'))
            Session::put('idc',$request->idc);
        else
            Session::put('idc',0);

        if($request->has('idt'))
            Session::put('idt',$request->idt);
        else
            Session::put('idt',0);

        if($request->has('page'))
            Session::put('page',$request->page);
        else
            Session::put('page',1);


        $http = new Client();

        //Categorias HEader
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        //Productos View
        $response2 = $http->get(env('API_URL',$this->urlLocal).'api/productosPromoFirst', [
            'query' => [
                'idc' => Session::get('idc',0),
                'idt' => Session::get('idt',0),
                'page' => Session::get('page',1)
                ]
        ]);
        $productos = json_decode((string) $response2->getBody(), true);

        return view('principal.promociones',compact('categorias','productos'));
    }

    public function marcas(Request $request){
        if(!$request->has('idm'))
            return redirect('/');
        else
            Session::put('idm',$request->idm);

        if($request->has('idc') && $request->idc > 0){
            Session::put('idc',$request->idc);
        }else{
            Session::put('idc',0);
        }

        if($request->has('idt') && $request->idt > 0){
            Session::put('idt',$request->idt);
        }else{
            Session::put('idt',0);
        }

        if($request->has('page'))
            Session::put('page',$request->page);

        $http = new Client();
        //Categorias HEader
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        //Productos View
        $response2 = $http->get(env('API_URL',$this->urlLocal).'api/productosMarcasFirst', [
            'query' => [
                'idm' => Session::get('idm',0),
                'idc' => Session::get('idc',0),
                'idt' => Session::get('idt',0),
                ]
        ]);
        $productos = json_decode((string) $response2->getBody(), true);

        return view('principal.list_products_marcas',compact('categorias','productos'));
    }

    public function template(){
        return view('template.index');
    }

    public function quick_view(){
        return view('principal.quick_view');
    }

    public function show_product_detail(){
        return view('principal.show_product_detail');
    }

    public function getToken(){
        $http = new Client();

        $response = $http->post(env('API_URL',$this->urlLocal).'oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'uFcGBVO9JdkrRRhO6AKTsDo15BGadUSZ3xP0pp81',
                'username' => 'darky',
                'password' => 'descent2702',
                'scope' => '*',
            ],
        ]);

        Session::put('api_token', json_decode((string) $response->getBody(), true));
    }

    public function getProductosList(Request $request){
        $http = new Client();

        $response2 = $http->get(env('API_URL',$this->urlLocal).'api/listProductos', [
            'query' => [
                'idf' => Session::get('idf',0),
                'idc' => Session::get('idc',0),
                'idt' => Session::get('idt',0),
                'page' => Session::get('page',1)
                ]
        ]);
        $productos = json_decode((string) $response2->getBody(), true);

        return view('layout.componentes.products_list',compact('productos'));

    }

    public function getProductosListPromocion(Request $request){
        $http = new Client();

        $response2 = $http->get(env('API_URL',$this->urlLocal).'api/listProductosPromo', [
            'query' => [
                'idf' => Session::get('idf',0),
                'idc' => Session::get('idc',0),
                'idt' => Session::get('idt',0),
                'page' => Session::get('page',1)
                ]
        ]);
        $productos = json_decode((string) $response2->getBody(), true);

        return view('layout.componentes.products_list',compact('productos'));

    }

    public function getProductosListMarca(Request $request){
        $http = new Client();

        $response2 = $http->get(env('API_URL',$this->urlLocal).'api/listProductosMarcas', [
            'query' => [
                'idm' => Session::get('idm',1),
                'idc' => Session::get('idc',0),
                'idt' => Session::get('idt',0),
                ]
        ]);
        $productos = json_decode((string) $response2->getBody(), true);

        return view('layout.componentes.products_list',compact('productos'));

    }

    public function vistaRapida($id){
        $http = new Client();
        $response = $http->get(env('API_URL',$this->urlLocal).'api/getProducto/'.$id);
        $producto = json_decode((string) $response->getBody());
        if(!empty($producto->Imagenes))
        $producto->Imagenes = collect($producto->Imagenes);
        else
        $producto->Imagenes = null;

        return view('layout.componentes.quick_view',compact('producto'));
    }

    public function show($id){
        $http = new Client();
        //Categorias HEader
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        $response = $http->get(env('API_URL', $this->urlLocal).'api/getProducto/'.$id);
        $producto = json_decode((string) $response->getBody());
        if(!empty($producto->Imagenes))
        $producto->Imagenes = collect($producto->Imagenes);
        else
        $producto->Imagenes = null;


        return view('principal.producto',compact('categorias','producto'));
    }

    public function carrito(){
        $http = new Client();
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('principal.cart',compact('categorias'));
    }

    public function checkout(){
        $http = new Client();
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL',$this->urlLocal).'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('principal.checkout',compact('categorias'));
    }
}
