<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PaginasController extends Controller
{
    public function nosotros(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.nosotros',compact('categorias'));
    }

    public function preguntasFrecuentes(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.preguntas_frecuentes',compact('categorias'));
    }

    public function cuidadoCalzado(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.cuidado_calzado',compact('categorias'));
    }

    public function cuidadoPies(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.cuidado_pies',compact('categorias'));
    }

    public function politicas(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.politicas',compact('categorias'));
    }

    public function terminos(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.terminos',compact('categorias'));
    }

    public function avisoPrivacidad(){
        $http = new Client();

        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('paginas.aviso_privacidad',compact('categorias'));
    }
}
