<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $http = new Client();
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }
        return view('auth.passwords.email',compact('categorias'));
    }
}
