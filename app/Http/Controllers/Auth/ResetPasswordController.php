<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Session;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function showResetForm(Request $request, $token = null)
    {
        if(!Session::has('HeaderCategorias') || Session::get('HeaderCategorias',null) == null){
            $http = new Client();
            $response = $http->get(env('API_URL','http://localhost/stilet/').'api/categorias');
            $categorias = json_decode((string) $response->getBody(), true);
            Session::put('HeaderCategorias',$categorias);
        }else{
            $categorias = Session::get('HeaderCategorias',null);
        }

        return view('auth.passwords.reset',compact('categorias'))->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
