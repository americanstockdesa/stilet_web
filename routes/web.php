<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Principal
Route::get('/','PrincipalController@index')->name('home');

//Rutas de Paginas Generales
Route::get('nosotros','PaginasController@nosotros');
Route::get('preguntas_frecuentes','PaginasController@preguntasFrecuentes');
Route::get('cuidado_calzado','PaginasController@cuidadoCalzado');
Route::get('cuidado_pies','PaginasController@cuidadoPies');
Route::get('politicas','PaginasController@politicas');
Route::get('terminos','PaginasController@terminos');
Route::get('aviso_privacidad','PaginasController@avisoPrivacidad');

//Rutas de Pago
Route::get('carrito','PrincipalController@carrito');
Route::get('checkout','PrincipalController@checkout');

//Rutas de Productos
Route::prefix('productos')->group(function(){
    Route::get('/','PrincipalController@productos');
    Route::get('show/{id}','PrincipalController@show');
    Route::get('get_productos','PrincipalController@getProductosList');

    Route::get('marcas','PrincipalController@marcas');
    Route::get('get_productos_marca','PrincipalController@getProductosListMarca');

    Route::get('promociones','PrincipalController@promociones');
    Route::get('get_productos_promocion','PrincipalController@getProductosListPromocion');

    Route::get('vista_rapida/{id}','PrincipalController@vistaRapida');
});

//Rutas Temporales
Route::get('/quick_view','PrincipalController@quick_view');
Route::get('/template','PrincipalController@template');
Route::get('/show_product_detail','PrincipalController@show_product_detail');

//Rutas Protegidas por login
Auth::routes();
Route::middleware('auth')->group(function () {

});
