@extends('layout.app')

@section('css')
@include('layout.css.productos')
@endsection

@section('body')
    @include('layout.header',compact('categorias'))
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-4 my-5">
                <h1 class="display-4"><strong>PREGUNTAS FRECUENTES</strong></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-4 my-5">
                <h1 class="display-4">&nbsp;</h1>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-6">
                <a data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1">1. ¿QUIÉNES SOMOS?</a>
                <div class="collapse" id="collapse1">
                    <div class="card card-body">
                        Stilet es la zapatería con el área de exhibición más grande y moderna de la región costa sur del estado de Jalisco con más de 600 estilo en exhibición, ubicada en Autlán de Navarro. Iniciamos operaciones en el año 2009, ofreciendo calzado de moda para toda la familia. Contamos con calzado para toda ocasión, manejando línea de damas, caballeros y niños, de las mejores marcas nacionales e internacionales.
                        <br>
                        <br>
                        En Stilet encontrarás desde zapatos de fiesta, zapatillas, sandalias, botas, botines y flats, hasta mocasines, tenis y calzado escolar. Además, podrás encontrar accesorios como bolsos, carteras, monederos, cinturones y gran variedad de productos de limpieza para el calzado.
                    </div>
                </div>

                <br>
                <a data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2">2. ¿CUÁL ES NUESTRO DOMICILIO?</a>
                <div class="collapse" id="collapse2">
                    <div class="card card-body">
                        Nuestra zapatería se encuentra ubicada en Calle Corona Araiza #6, frente al jardín del kiosko, en el municipio de Autlán de Navarro, Jalisco, México.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse3" role="button" aria-expanded="false" aria-controls="collapse3">3. ¿POR QUÉ MEDIOS PUEDO PONERME EN CONTACTO CON PERSONAL DE STILET?</a>
                <div class="collapse" id="collapse3">
                    <div class="card card-body">
                        Puedes ponerte en contacto con nosotros a través de los siguientes medios:<br>
                        • Teléfono: 317 381 4111<br>
                        • WhatsApp: 317 104 9375 <br>
                        • Correo electrónico: contacto@stilet.com.mx
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse4" role="button" aria-expanded="false" aria-controls="collapse4">4. ¿DÓNDE PUEDO BUSCAR UN PRODUCTO?</a>
                <div class="collapse" id="collapse4">
                    <div class="card card-body">
                        Selecciona en el menú superior si el producto que buscas es para dama, caballero o niños. Una vez seleccionado la familia, se desplegará un menú con las categorías de productos que se tienen para dicho segmento. También puedes realizar una búsqueda por tipo de calzado, rango de precios, talla o color.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse5" role="button" aria-expanded="false" aria-controls="collapse5">5. ¿CÓMO ME REGISTRO?</a>
                <div class="collapse" id="collapse5">
                    <div class="card card-body">
                        Registrarte es muy sencillo, solo tienes que dar clic en el icono de Mi Cuenta que se encuentra encima de la barra del menú y posteriormente dar clic en Registrarse donde completaras un formulario y listo. <br><br>
                        Después de registrarte recibirás un email de confirmación con todos tus datos. Verifica que tus datos sean correctos y no olvides dar clic en el link que te endviamos para confirmar tu registro. <br><br>
                        Te invitamos a conocer nuestro aviso de privacidad aquí.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse6" role="button" aria-expanded="false" aria-controls="collapse6">6. ¿CÓMO PUEDO RECUPERAR MI CONTRASEÑA SI LA PERDÍ O LA OLVIDÉ?</a>
                <div class="collapse" id="collapse6">
                    <div class="card card-body">
                        Solo tienes que dar clic en el icono de Mi Cuenta que se encuentra encima de la barra del menú y posteriormente dar clic en ¿Olvidaste la contraseña? <br><br>
                        Una vez dentro de la sección, deberás escribir el correo electrónico con el que te registraste, a donde  te enviaremos un email con la nueva contraseña para poder acceder a tu cuenta.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse7" role="button" aria-expanded="false" aria-controls="collapse7">7. ¿DÓNDE PUEDO MODIFICAR MIS DATOS?</a>
                <div class="collapse" id="collapse7">
                    <div class="card card-body">
                        En todo momento tendrás acceso a modificar los datos de tu cuenta como el email, dirección de entrega, teléfono de contacto etc. Para modificar esta información solo tienes que iniciar sesión y posteriormente dar clic en “direcciones” o “detalles cuenta” según la información que se quieres modificar.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse8" role="button" aria-expanded="false" aria-controls="collapse8">8. ¿CÓMO PUEDO REALIZAR UNA COMPRA EN LA TIENDA ONLINE DE STILET®?</a>
                <div class="collapse" id="collapse8">
                    <div class="card card-body">
                        Las compras realizadas a través de nuestra tienda online son fáciles y seguras, para realizar tu compra de forma exitosa te recomendamos seguir los siguientes pasos: <br><br>
                            • Antes de iniciar todo el proceso de compra te sugerimos realizar tu registro en nuestra tienda online. <br><br>

                            • Si quieres ver todos los productos que tenemos disponibles selecciona el tipo de producto desde nuestro menú. <br><br>

                            • Si buscas algún producto en específico del lado izquierdo encontraras una barra de filtros que te facilitara la búsqueda. Puedes realizar una búsqueda por tipo de calzado, rango de precios, talla o color. <br><br>

                            • Al dar clic en la foto del producto que te interesa podrás ver los detalles del modelo. <br><br>

                            • Para iniciar el proceso de compra da clic en el botón de “añadir a carrito” y el producto se agregara automáticamente a tu carrito de compras en el color y talla seleccionada. <br><br>

                            • Una vez que estés segura de tener todos los artículos que te interesan en el carrito de compras es momento de finalizar la compra. Para finalizarla hay que dar clic en el icono de la bolsa de compras que encuentra en la parte superior derecha de la página. <br><br>

                            • Aparecerá un resumen de tu compra indicando cantidad, modelo descripción y precio. Si todos los datos son correctos hay que dar clic en finalizar la compra. <br><br>

                            • Si ya estas registrado solo necesitas iniciar sesión con tu email y contraseña. En caso de no estar registrado del lado derecho te daremos la opción para el registro. <br><br>

                            • Es momento de confirmar que todos tus datos sean correctos, si los datos son correctos solo tienes que dar clic en “finalizar compra”. <br><br>

                            • Llegamos al resumen de la compra donde podrás ver a detalle tu pedido. En caso de tener un cupón de descuento no olvides utilizarlo. Da clic en finalizar compra. <br><br>

                            • Una vez que finalices el proceso aparecerá en pantalla la confirmación de que la compra fue realiza con éxito y también recibirás un email de confirmación. <br><br>

                            Si tienes alguna duda adicional nos puedes contactar al  317 381 4111 o envíanos un email a contacto@stilet.com.mx y nos pondremos en contacto a la brevedad.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse9" role="button" aria-expanded="false" aria-controls="collapse9">9. ¿DÓNDE PUEDO HACER EL SEGUIMIENTO DE MI PEDIDO?</a>
                <div class="collapse" id="collapse9">
                    <div class="card card-body">
                        Accede a Mi cuenta con tu usuario y contraseña. Una vez que estés en tu cuenta da clic en “pedidos” donde podrás ver el estado de tu pedido. Para cualquier duda adicional también puedes contactarnos vía telefónica o por email.
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <a data-toggle="collapse" href="#collapse10" role="button" aria-expanded="false" aria-controls="collapse10">10. ¿CUÁL ES EL COSTO DE ENVÍO?</a>
                <div class="collapse" id="collapse10">
                    <div class="card card-body">
                        En las compras mayores a $999 MXN el envío es gratis a cualquier parte de la República Mexicana. En compras menores a $999 MXN el envío tendrá un costo de $150 MXN.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse11" role="button" aria-expanded="false" aria-controls="collapse11">11. ¿TENGO DUDAS SOBRE LA PAQUETERÍA YFECHAS DE ENTREGA?</a>
                <div class="collapse" id="collapse11">
                    <div class="card card-body">
                        Todos nuestros envíos se realizan a través de ESTAFETA, manejando un tiempo estimado de entrega de entre 2 y 3 días hábiles. El tiempo de entrega puedo variar en zonas remotas.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse12" role="button" aria-expanded="false" aria-controls="collapse12">12. ¿DÓNDE PUEDO RASTREAR EL ENVÍO DE MI PEDIDO?</a>
                <div class="collapse" id="collapse12">
                    <div class="card card-body">
                        Para rastrear tu pedido es necesario tener a la mano el número de guía e ingresar a https://www.estafeta.com/Herramientas/RastreoV1 donde podrás consultar el avance del envío.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse13" role="button" aria-expanded="false" aria-controls="collapse13">13. ¿CÓMO HAGO EL CAMBIO O DEVOLUCIÓN DE UN PEDIDO?</a>
                <div class="collapse" id="collapse13">
                    <div class="card card-body">
                        Tipos de cambio por estilo, talla (15 días) o garantía (30 días sobre buen uso). Modulo de Cambios/Modulo de Garantías <br><br>
                        La solicitud de cambio por talla o modelo o devolución debe realizarse dentro de un máximo de 10 días naturales posteriores a la entrega, comunicándote a nuestro teléfono de atención a clientes 317 381 4111, mencionando el número de pedido y  la explicación del tipo de cambio solicitado, así como el modelo o talla que deseas recibir a cambio. <br><br>
                        Revisaremos la solicitud, y una vez autorizada te enviaremos por correo electrónico una CLAVE DE ENVÍO, la cual debe incluirse en la “Forma de Devolución”. Se debe entregar el producto sin usarse, en la caja original con la que fue entregada la mercancía y sin daños; a la oficina de ESTAFETA que se indique, anexando forzosamente la “Forma de Devolución”.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse14" role="button" aria-expanded="false" aria-controls="collapse14">14. ¿LOS PRODUCTOS CUENTAN CON GARANTÍA POR DEFECTO DE FABRICACIÓN?</a>
                <div class="collapse" id="collapse14">
                    <div class="card card-body">
                        En caso de mercancía con defecto de fabricación, se debe reportar dentro de los primeros 30 días naturales posteriores a su entrega, comunicándote a nuestro teléfono de atención a clientes 317 381 411, mencionando el número de pedido y el tipo de daño presentado en el producto, así como una fotografía que demuestre el defecto de fabricación. <br><br>
                        Revisaremos la solicitud, y una vez autorizada enviaremos por correo electrónico una CLAVE DE ENVÍO, la cual debe incluirse en la “Forma de Devolución”. Se debe entregar el producto sin usarse, en la caja original con la que fue entregada la mercancía y sin daños; a la oficina de ESTAFETA que se indique, anexando forzosamente la “Forma de Devolución”.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse15" role="button" aria-expanded="false" aria-controls="collapse15">15. ¿QUÉ MÉTODOS DE PAGO SE ACEPTAN?</a>
                <div class="collapse" id="collapse15">
                    <div class="card card-body">
                        Aceptamos pagos  con tarjeta de crédito y débito de todas las instituciones bancarias del país. También pagos a través de PayPal con cualquier tarjeta de crédito o débito y pagos en efectivo a través de MercadoPago en OXXO, Banamex, Santander y BBVA. Los pagos en efectivo realizados a través de OXXO, Banamex, Santander y BBVA, tardan de 2 a 3 días hábiles en acreditarse. <br><br>
                        También se puede hacer deposito o transferencia a nuestra cuenta bancaria la cual estará visible al momento de finalizar la compra y elegir como método de pago Transferencia Bancaria directa.  Para que los pagos realizados a través de este medio sean contados como recibidos es necesario enviar el comprobante de pago por WhatApp al teléfono 317 104 9375. <br><br>
                        Tienes un plazo máximo de 2 días hábiles después de haber levantado el pedido para realizar el pago.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse16" role="button" aria-expanded="false" aria-controls="collapse16">16. ¿POR QUÉ SE RECHAZÓ MI PAGO CON TARJETA DE CRÉDITO O DÉBITO?</a>
                <div class="collapse" id="collapse16">
                    <div class="card card-body">
                        Existen varios motivos por los cuales tu pago puede ser rechazado, te recomendamos ponerte en contacto con tu banco para saber si tu cuenta tiene algún tipo de bloqueo para compras online, así como para la aclaración de cualquier duda. <br><br>
                        Es importante asegurarte de que tus datos estén capturados correctamente antes de finalizar la compra.
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse17" role="button" aria-expanded="false" aria-controls="collapse17">17. ¿CÓMO APLICAR UNA PROMOCIÓN A MI COMPRA?</a>
                <div class="collapse" id="collapse17">
                    <div class="card card-body">
                        Deberás aplicar el cupón de promoción o cualquier otro cupón antes de finalizar la compra, ya que una vez finalizada es imposible aplicarlo. <br><br>
                        Cualquier cupón debe ser aplicado dentro del proceso de compra cuando te encuentres en el resumen del “carrito de compras”. Una vez en esa sección encontraras un recuadro que dice “código de cupón” donde deberás introducir el código del cupón y dar clic en “aplicar cupón”. Una vez aplicado,  se verá reflejado el descuento del cupón en el resumen del carrito de compra del lado derecho. <br><br>
                        Los cupones pueden contar con las siguientes restricciones: <br><br>
                            • Solo aplica en productos seleccionados. <br>
                            • Monto mínimo de compra. <br>
                            • Sujeto a disponibilidad. <br>
                            • Vigencia. <br>
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse18" role="button" aria-expanded="false" aria-controls="collapse18">18. ¿CUÁLES SON LOS TÉRMINOS LEGALES DE LAS PROMOCIONES?</a>
                <div class="collapse" id="collapse18">
                    <div class="card card-body">
                        • En producto con descuento o rebajado no aplican cambios o devoluciones. <br>
                        • Promociones no acumulables con otras promociones. <br>
                        • Promociones sujetas a cambios sin previo aviso. <br>
                        • Los precios pueden variar de página web a tienda física. <br>
                        • Pregunta por modelos participantes. <br><br>

                        Para mayor información consulta nuestros términos y condiciones o envía un correo a contacto@stilet.com.mx
                    </div>
                </div>
                <br>
                <a data-toggle="collapse" href="#collapse19" role="button" aria-expanded="false" aria-controls="collapse19">19. ¿CUÁLES SON LOS BENEFICIIOS DE SUSCRIBIRME AL NEWSLETTER?</a>
                <div class="collapse" id="collapse19">
                    <div class="card card-body">
                        Si te suscribes a nuestro newsletter podrás aprovechar la disponibilidad de los nuevos modelos antes que nadie; también te mantendremos al tanto de nuestras ofertas y promociones especiales.
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layout.componentes.banner_info')



    @include('layout.footer')

    @endsection
