@extends('layout.app')

@section('css')
@include('layout.css.productos')
@endsection

@section('body')
    @include('layout.header',compact('categorias'))
    <div class="container">
        <div class="row">
            <div class="col-md-9 offset-md-3 mb-5">
                <img class="img-fluid rounded" src="{{ asset('images/bannerNosotros.png') }}">
            </div>
        </div>
        <div class="row mt-5">
            <h1 class="display-4"><strong>POLITICAS</strong></h1>
        </div>
        <div class="row">

        </div>
    </div>
    @include('layout.componentes.banner_info')



    @include('layout.footer')

    @endsection
