@extends('layout.app')

@section('css')
@include('layout.css.productos')
@endsection

@section('body')
    @include('layout.header',compact('categorias'))
    <div class="container">
        <div class="row">
            <div class="col-md-9 offset-md-3 mb-5">
                <img class="img-fluid rounded" src="{{ asset('images/bannerNosotros.png') }}">
            </div>
        </div>
        <div class="row mt-5">
            <h1 class="display-4"><strong>SOMOS STILET</strong></h1>
        </div>
        <div class="row">
            <p>
                Stilet es la zapatería con el área de exposición más grande y moderna de la región costa sur del estado de Jalisco
                (más de 600 modelos en exhibición), ubicada en Autlán de Navarro. Iniciamos operaciones en el año 2009, ofreciendo
                calzado de moda para toda la familia. Contamos con calzado para toda ocasión, manejando línea para damas,
                caballeros y niños, de las mejores marcas nacionales e internacionales.
                <br>
                <br>
                En Stilet encontrarás desde zapatos de fiesta, zapatillas, sandalias, botas, botines y flats, hasta mocasines, tenis y calzado
                escolar. Además, podrás encontrar accesorios como bolsos, carteras, monederos, cinturones y gran variedad de
                productos de limpieza para el calzado.
            </p>
        </div>
    </div>
    @include('layout.componentes.banner_info')



    @include('layout.footer')

    @endsection
