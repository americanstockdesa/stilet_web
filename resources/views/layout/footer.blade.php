<!-- START FOOTER -->
<footer class="footer_dark">
	<div class="footer_top footNew mb-0 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                	<div class="widget">
                        <div class="footer_logo my-0 py-0">
                            <a href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}" height="100px" alt="logo"/></a>
                        </div>
                        <p class="footLetterNew pl-md-4 mt-2"><b>
                            Corona Araiza #6 Autlán de Navarro Jalisco, México.
                            <br><br>
                            <a class="footLetterNew footLetterNewLink" href="tel:3171214822">317 121 4822</a>
                            <br>
                            <a class="footLetterNew footLetterNewLink" href="mailto:contacto@stilet.com.mx">contacto@stilet.com.mx</a>
                        </b></p>
                    </div>
                    {{-- <div class="widget">
                        <ul class="social_icons social_white">
                            <li><a href="#"><i class="footLetterNew ion-social-facebook"></i></a></li>
                            <li><a href="#"><i class="footLetterNew ion-social-twitter"></i></a></li>
                            <li><a href="#"><i class="footLetterNew ion-social-googleplus"></i></a></li>
                            <li><a href="#"><i class="footLetterNew ion-social-youtube-outline"></i></a></li>
                            <li><a href="#"><i class="footLetterNew ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div> --}}
                </div>
                @php
                    $actual = str_replace(url('/').'/','',request()->url());
                @endphp
                <div class="col-lg-4 col-md-6 col-sm-6 pl-md-5 ml-md-5">
                	<div class="widget">
                        <h6 class="widget_title footLetterNew"><b>Informacion</b></h6>
                        <ul class="widget_links">
                            <li><b><a href="{{ url('nosotros') }}" class="footLetterNew footLetterNewLink @if($actual == 'nosotros') LinkActivo @endif">Acerca de Nosotros</a></b></li>
                            <li><b><a href="{{ url('preguntas_frecuentes') }}" class="footLetterNew footLetterNewLink @if($actual == 'preguntas_frecuentes') LinkActivo @endif">Preguntas Frecuentes</a></b></li>
                            <li><b><a href="{{ url('politicas') }}" class="footLetterNew footLetterNewLink @if($actual == 'politicas') LinkActivo @endif">Políticas de Cambios y Devoluciones</a></b></li>
                            <li><b><a href="{{ url('terminos') }}" class="footLetterNew footLetterNewLink @if($actual == 'terminos') LinkActivo @endif">Terminos y Condiciones</a></b></li>
                            <li><b><a href="{{ url('aviso_privacidad') }}" class="footLetterNew footLetterNewLink @if($actual == 'aviso_privacidad') LinkActivo @endif">Aviso de Privacidad</a></b></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 ml-md-4">
                	<div class="widget">
                        <h6 class="widget_title footLetterNew"><b>Mi Cuenta</b></h6>
                        <ul class="widget_links">
                            <li><a href="{{ url('register') }}" class="footLetterNew footLetterNewLink @if($actual == 'register') LinkActivo @endif"><b>Crear Cuenta</b></a></li>
                            <li><a href="{{ url('login') }}" class="footLetterNew footLetterNewLink @if($actual == 'login') LinkActivo @endif"><b>Iniciar Sesión</b></a></li>
                            <li><a href="{{ url('deseos') }}" class="footLetterNew footLetterNewLink @if($actual == 'deseos') LinkActivo @endif"><b>Lista de Deseos</b></a></li>
                            <li><a href="{{ url('carrito') }}" class="footLetterNew footLetterNewLink @if($actual == 'carrito') LinkActivo @endif"><b>Carrito de Compra</b></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_footer border-top-tran SubFootNew pt-0 mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="footer_payment text-center">
                        <li><a href="#" class="mx-md-3"><img src="{{ asset('images/visa_new.png') }}" alt="visa"></a></li>
                        <li><a href="#" class="mx-md-3"><img src="{{ asset('images/mastercard.png') }}" alt="master_card"></a></li>
                        <li><a href="#" class="mx-md-3"><img src="{{ asset('images/oxxo.png') }}" alt="oxxo"></a></li>
                        <li><a href="#" class="mx-md-3"><img src="{{ asset('images/paypal_new.png') }}" alt="paypal"></a></li>
                        <li><a href="#" class="mx-md-3"><img src="{{ asset('images/mercadopago.png') }}" alt="mercadopago"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->
