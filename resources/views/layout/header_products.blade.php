<!-- START HEADER -->
<header class="header_wrap fixed-top header_with_topbar">
	<div class="top-header d-none d-md-block headAmarillo">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-8">
                	<div class="header_topbar_info">
                    	<b><span class="mr-1 headLetterNew">Envíos Gratis</span><span> en compras mayores a $999</span></b>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4">
                	&nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_header dark_skin main_menu_uppercase bgTop">
    	<div class="container">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="logo_dark" src="{{ asset("images/logo.png") }}" height="40px" alt="logo" />
                </a>
                <button class="navbar-toggler text-danger" type="button" data-toggle="collapse" data-target="#navbarSupportedContent2" aria-expanded="false">
                    <span class="ion-android-menu"></span>
                </button>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false">
                    <span class="ion-android-menu text-white"></span>
                </button>
                @php
                    $actual = str_replace(url('/').'/','',request()->url());
                @endphp
                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <ul class="navbar-nav bgTop">
                        <li><a class="nav-link nav_item headLink" href="{{ url('/') }}">INICIO</a></li>
                        <li><a class="nav-link nav_item headLink @if($actual == 'productos/promociones') LinkActivo @endif" href="{{ url('productos/promociones') }}">PROMOCIONES</a></li>
                        <li><a class="nav-link nav_item headLink @if($actual == 'nosotros') LinkActivo @endif"  href="{{ url('nosotros') }}">NOSOTROS</a></li>
                        <li><a class="nav-link nav_item headLink @if($actual == 'preguntas_frecuentes') LinkActivo @endif" href="{{ url('preguntas_frecuentes') }}">PREGUNTAS FRECUENTES</a></li>
                        @if(!Auth::check())
                        <li><a class="nav-link nav_item headLink my-0" href="{{ url('login') }}" style="background-color: #fef2d4 !important; color: #c55844 !important">REGISTRO / LOGIN</a></li>
                        @endif
                    </ul>
                </div>
                <ul class="navbar-nav attr-nav align-items-center">
                    <li><a href="javascript:void(0);" class="nav-link search_trigger text-white"><i class="linearicons-magnifier"></i></a>
                        <div class="search_wrap">
                            <span class="close-search"><i class="ion-ios-close-empty"></i></span>
                            <form>
                                <input type="text" placeholder="Buscar" class="form-control" id="search_input">
                                <button type="submit" class="search_icon"><i class="ion-ios-search-strong"></i></button>
                            </form>
                        </div><div class="search_overlay"></div>
                    </li>
                    @if(Auth::check())
                        <li><a href="#" class="nav-link text-white"><i class="linearicons-user"></i></a></li>
                        <li><a href="#" class="nav-link text-white"><i class="linearicons-heart"></i><span class="wishlist_count">0</span></a></li>
                    @endif

                    <li class="dropdown cart_dropdown"><a class="nav-link cart_trigger text-white" href="#" data-toggle="dropdown"><i class="linearicons-cart"></i><span class="cart_count">2</span></a>
                        <div class="cart_box dropdown-menu dropdown-menu-right">
                            <ul class="cart_list">
                                <li>
                                    <a href="#" class="item_remove"><i class="ion-close"></i></a>
                                    <a href="#"><img src="{{ asset("images/cart_thamb1.jpg") }}" alt="cart_thumb1">Variable product 001</a>
                                    <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>78.00</span>
                                </li>
                                <li>
                                    <a href="#" class="item_remove"><i class="ion-close"></i></a>
                                    <a href="#"><img src="{{ asset("images/cart_thamb2.jpg") }}" alt="cart_thumb2">Ornare sed consequat</a>
                                    <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>81.00</span>
                                </li>
                            </ul>
                            <div class="cart_footer">
                                <p class="cart_total"><strong>Subtotal:</strong> <span class="cart_price"> <span class="price_symbole">$</span></span>159.00</p>
                                <p class="cart_buttons"><a href="{{ url('carrito') }}" class="btn btn-fill-line view-cart">View Cart</a><a href="{{ url('checkout') }}" class="btn btn-fill-out checkout">Checkout</a></p>
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="bottom_header dark_skin main_menu_uppercase .d-none .d-sm-block">
    	<div class="container">
            <nav class="navbar navbar-expand-lg">
                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent2">
                    <ul class="navbar-nav">
                        @isset($categorias[1])
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="nav-link dropdown-toggle" href="#">ELLAS</a>
                            <div class="dropdown-menu">
                                <ul>
                                    @foreach($categorias[1] as $categoria)
                                    <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=1&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                    @endforeach()
                                </ul>
                            </div>
                        </li>
                        @endisset
                        @isset($categorias[2])
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">ELLOS</a>
                            <div class="dropdown-menu">
                                <ul>
                                    @foreach($categorias[2] as $categoria)
                                        <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=2&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                    @endforeach()
                                </ul>
                            </div>
                        </li>
                        @endisset
                        @isset($categorias[4])
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">NIÑAS</a>
                            <div class="dropdown-menu">
                                <ul>
                                    @foreach($categorias[4] as $categoria)
                                        <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=4&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                    @endforeach()
                                </ul>
                            </div>
                        </li>
                        @endisset
                        @isset($categorias[3])
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">NIÑOS</a>
                            <div class="dropdown-menu">
                                <ul>
                                    @foreach($categorias[3] as $categoria)
                                        <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=3&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                    @endforeach()
                                </ul>
                            </div>
                        </li>
                        @endisset
                        @isset($categorias[5])
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">BEBÉS</a>
                            <div class="dropdown-menu">
                                <ul>
                                    @foreach($categorias[5] as $categoria)
                                        <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=5&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                    @endforeach()
                                </ul>
                            </div>
                        </li>
                        @endisset
                        @isset($categorias[99])
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">ACCESORIOS</a>
                            <div class="dropdown-menu">
                                <ul>
                                    @foreach($categorias[99] as $categoria)
                                        <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=99&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                    @endforeach()
                                </ul>
                            </div>
                        </li>
                        @endisset
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- END HEADER -->
