@php
    $idf = Session::get('idf',0);
@endphp

<!-- START HEADER -->
<header class="header_wrap">
    <div class="top-header d-none d-md-block headAmarillo">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-8">
                	<div class="header_topbar_info">
                    	<b><span class="mr-1 headLetterNew">Envíos Gratis</span><span> en compras mayores a $999</span></b>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4">
                	&nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="middle-header dark_skin">
    	<div class="container">
        	<div class="nav_block">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img class="logo_dark" src="{{ asset('images/logo.png') }}" height="60px" alt="logo" />
                </a>
                <div class="product_search_form">
                    <form>
                        <div class="input-group">
                            <input class="form-control" placeholder="Busca tu producto" required=""  type="text">
                            <button type="submit" class="search_btn"><i class="linearicons-magnifier"></i></button>
                        </div>
                    </form>
                </div>
                <div class="contact_phone order-md-last">
                    <img src="{{ asset('images/whatsapp_logo.png') }}" height="40px" alt="whatsapp" />
                    <img src="{{ asset('images/messenger.png') }}" height="40px" alt="messenger" />
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_header light_skin main_menu_uppercase bgTop mb-4">
    	<div class="container">
            <div class="row">
            	<div class="col-lg-3 col-md-4 col-sm-6 col-3">
                	<div class="categories_wrap">
                        <button type="button" data-toggle="collapse" data-target="#navCatContent" aria-expanded="false" class="categories_btn">
                            <i class="linearicons-menu"></i><span>NUESTROS PRODUCTOS </span>
                        </button>
                        <div id="navCatContent" class="nav_cat navbar collapse">
                            <ul>
                                @isset($categorias[1])
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><i class="flaticon-woman"></i> <span>PARA ELLAS</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex">
                                            <li class="mega-menu-col col-lg-12">
                                                <ul class="d-lg-flex">
                                                    <li class="mega-menu-col col-lg-12">
                                                        <ul>
                                                            {{-- <li class="dropdown-header"></li> --}}
                                                            @foreach($categorias[1] as $categoria)
                                                            <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=1&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                                            @endforeach()
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                @endisset
                                @isset($categorias[2])
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><i class="flaticon-boss"></i> <span>PARA ELLOS</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex">
                                            <li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex">
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul>
                                                            @foreach($categorias[2] as $categoria)
                                                            <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=2&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                                            @endforeach()
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endisset
                                @isset($categorias[4])
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><span class="iconSVG iconSVG-girl"></span> <span>PARA NIÑAS</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex">
                                            <li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex">
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul>
                                                            @foreach($categorias[4] as $categoria)
                                                            <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=4&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                                            @endforeach()
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endisset
                                @isset($categorias[3])
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler mt-1" href="#" data-toggle="dropdown"><span class="iconSVG iconSVG-boy"></span> <span>PARA NIÑOS</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex">
                                            <li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex">
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul>
                                                            @foreach($categorias[3] as $categoria)
                                                            <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=3&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                                            @endforeach()
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endisset
                                @isset($categorias[5])
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><span class="iconSVG iconSVG-baby"></span> <span>PARA BEBÉS</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex">
                                            <li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex">
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul>
                                                            @foreach($categorias[5] as $categoria)
                                                            <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=5&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                                            @endforeach()
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endisset
                                @isset($categorias[99])
                                <li class="dropdown dropdown-mega-menu">
                                    <a class="dropdown-item nav-link dropdown-toggler" href="#" data-toggle="dropdown"><span class="iconSVG iconSVG-bag"></span> <span>ACCESORIOS</span></a>
                                    <div class="dropdown-menu">
                                        <ul class="mega-menu d-lg-flex">
                                            <li class="mega-menu-col col-lg-7">
                                                <ul class="d-lg-flex">
                                                    <li class="mega-menu-col col-lg-6">
                                                        <ul>
                                                            @foreach($categorias[99] as $categoria)
                                                            <li><a class="dropdown-item nav-link nav_item" href="{{ url('productos')."?idf=99&idc={$categoria['idCategoria']}" }}">{{ $categoria['Categoria'] }}</a></li>
                                                            @endforeach()
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                @endisset
                                <li><a class="dropdown-item nav-link nav_item" href="{{ url('cuidado_calzado') }}"><span>Cuidado Tu Calzado</span></a></li>
                                <li><a class="dropdown-item nav-link nav_item" href="{{ url('cuidado_pies') }}"><span>Cuidado para tus Pies</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-6 col-9">
                	<nav class="navbar navbar-expand-lg">
                    	<button class="navbar-toggler side_navbar_toggler" type="button" data-toggle="collapse" data-target="#navbarSidetoggle" aria-expanded="false">
                            <span class="ion-android-menu"></span>
                        </button>
                        @php
                            $actual = str_replace(url('/').'/','',request()->url());
                        @endphp
                        <div class="collapse navbar-collapse mobile_side_menu" id="navbarSidetoggle">
                            <ul class="navbar-nav">
                                <li><a class="nav-link nav_item mx-3" href="{{ url('productos/promociones') }}">PROMOCIONES</a></li>
                                <li><a class="nav-link nav_item mx-3 @if($actual == 'nosotros') LinkActivo @endif" href="{{ url('nosotros') }}">NOSOTROS</a></li>
                                <li><a class="nav-link nav_item mx-3 @if($actual == 'preguntas_frecuentes') LinkActivo @endif" href="{{ url('preguntas_frecuentes')}}">PREGUNTAS FRECUENTES</a></li>
                                @if(!Auth::check())
                                <li><a class="nav-link nav_item mx-3 pb-4" style="background-color: #fef2d4 !important; color: #c55844 !important" href="{{ url('login') }}">REGISTRO / LOGIN</a></li>
                                @endif
                            </ul>
                        </div>
                        <ul class="navbar-nav attr-nav align-items-center">
                            @if(Auth::check())
                                <li><a href="#" class="nav-link"><i class="linearicons-user"></i></a></li>
                                <li><a href="#" class="nav-link"><i class="linearicons-heart"></i><span class="wishlist_count">0</span></a></li>
                            @endif
                            <li class="dropdown cart_dropdown"><a class="nav-link cart_trigger" href="#" data-toggle="dropdown"><i class="linearicons-cart"></i><span class="cart_count">2</span></a>
                                <div class="cart_box dropdown-menu dropdown-menu-right">
                                    <ul class="cart_list">
                                        <li>
                                            <a href="#" class="item_remove"><i class="ion-close"></i></a>
                                            <a href="#"><img src="{{ asset('images/cart_thamb1.jpg') }}" alt="cart_thumb1">Variable product 001</a>
                                            <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>78.00</span>
                                        </li>
                                        <li>
                                            <a href="#" class="item_remove"><i class="ion-close"></i></a>
                                            <a href="#"><img src="{{ asset('images/cart_thamb2.jpg') }}" alt="cart_thumb2">Ornare sed consequat</a>
                                            <span class="cart_quantity"> 1 x <span class="cart_amount"> <span class="price_symbole">$</span></span>81.00</span>
                                        </li>
                                    </ul>
                                    <div class="cart_footer">
                                        <p class="cart_total"><strong>Subtotal:</strong> <span class="cart_price"> <span class="price_symbole">$</span></span>159.00</p>
                                        <p class="cart_buttons">
                                            <a href="{{ url('carrito') }}" class="btn btn-fill-line view-cart">View Cart</a>
                                            <a href="{{ url('checkout') }}" class="btn btn-fill-out checkout">Checkout</a></p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="pr_search_icon">
                        	<a href="javascript:void(0);" class="nav-link pr_search_trigger"><i class="linearicons-magnifier"></i></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- END HEADER -->
