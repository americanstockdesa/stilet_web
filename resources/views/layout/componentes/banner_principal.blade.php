<div class="banner_section slide_medium shop_banner_slider staggered-animation-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 offset-lg-3">
                <div id="carouselExampleControls" class="carousel slide light_arrow" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($bannersPrincipales as $banner)
                        @php
                            $activo = $loop->first;
                            $imagen = env('API2_URL','http://localhost/stilet/').'api/getBanner/'.$banner['id'];
                            $textoChico = $banner['TextoSecundario'];
                            $textoGrande = $banner['TextoPrincipal'];
                        @endphp
                        <div class="carousel-item @if($activo) active @endif background_bg" data-img-src="{{ $imagen }}">
                            <div class="banner_slide_content banner_content_inner">
                                <div class="col-lg-8 col-10">
                                    <div class="banner_content overflow-hidden">
                                        <h5 class="mb-3 staggered-animation font-weight-light" data-animation="slideInLeft" data-animation-delay="0.5s">{{ $textoChico }}</h5>
                                        <h2 class="staggered-animation" data-animation="slideInLeft" data-animation-delay="1s">{{ $textoGrande }}</h2>
                                        <a class="btn btn-fill-out rounded-0 staggered-animation text-uppercase" href="shop-left-sidebar.html" data-animation="slideInLeft" data-animation-delay="1.5s">Comprar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <ol class="carousel-indicators indicators_style1">
                        @foreach($bannersPrincipales as $banner)
                            <li data-target="#carouselExampleControls" data-slide-to="{{ $loop->index }}" @if($loop->first) class="active" @endif></li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
