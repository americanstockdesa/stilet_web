<!-- START SECTION SHOP -->
<div class="section small_pb nopad">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading_tab_header">
                    <div class="heading_s2">
                        <h2>Nuevas Colecciones</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row shop_container">
                    @foreach ($nuevasColecciones as $producto)
                    <div class="col-lg-3 col-md-4 col-6">
                        <div class="product">
                            <div class="product_img">
                                <a href="shop-product-detail.html">
                                    @if($producto['isFoto'])
                                        <img src="data:{{ $producto['FotoMime']}};base64, {{ $producto['Foto'] }}" alt="ProductImg">
                                    @else
                                        <img src="{{ asset('images/no_disponible.png') }}" alt="ProductImg">
                                    @endif
                                </a>
                                <div class="product_action_box">
                                    <ul class="list_none pr_action_btn">
                                        <li class="add-to-cart"><a href="{{ url('agregarCarrito/'.$producto['id']) }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                                        <li><a href="{{ url('productos/vista_rapida/'.$producto['id']) }}" class="popup-ajax"><i class="icon-magnifier-add"></i></a></li>
                                        @if(Auth::check())
                                        <li><a href="#"><i class="icon-heart"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="product_info">
                            <h6 class="product_title"><a href="{{ url('productos/show/'.$producto['id']) }}">{{ $producto['Categoria'] }} {{ $producto['Grupo'] }} {{ $producto['Color'] }} {{ $producto['Marca'] }}</a></h6>
                                <div class="product_price">
                                    <span class="price">${{ number_format($producto['PrecioVenta'],2) }}</span>
                                    {{-- <del>$55.25</del>
                                    <div class="on_sale">
                                        <span>35% Off</span>
                                    </div> --}}
                                </div>
                                <div class="rating_wrap">
                                    {{-- <div class="rating">
                                        <div class="product_rate" style="width:80%"></div>
                                    </div> --}}
                                    <span class="rating_num">{{ $producto['SKU'] }}</span>
                                </div>
                                <div class="pr_desc">
                                    <p>{{ $producto['Descripcion'] }}</p>
                                </div>
                                {{-- <div class="pr_switch_wrap">
                                    <div class="product_color_switch">
                                        <span class="active" data-color="#87554B"></span>
                                        <span data-color="#333333"></span>
                                        <span data-color="#DA323F"></span>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION SHOP -->
