<div class="row shop_container">
    @foreach($productos['data'] as $producto)
        <div class="col-md-4 col-6">
            <div class="product">
                <div class="product_img">
                    <a href="shop-product-detail.html">
                        @if($producto['isFoto'])
                            <img src="data:{{ $producto['FotoMime']}};base64, {{ $producto['Foto'] }}" alt="ProductImg">
                        @else
                            <img src="{{ asset('images/no_disponible.png') }}" alt="ProductImg">
                        @endif
                    </a>
                    <div class="product_action_box">
                        <ul class="list_none pr_action_btn">
                            <li class="add-to-cart"><a href="{{ url('agregarCarrito/'.$producto['id']) }}"><i class="icon-basket-loaded"></i> Agregar al Carrito</a></li>
                            <li><a href="{{ url('productos/vista_rapida/'.$producto['id']) }}" class="popup-ajax"><i class="icon-magnifier-add"></i></a></li>
                            @if(Auth::check())
                            <li><a href="#"><i class="icon-heart"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="product_info">
                    <h6 class="product_title"><a href="{{ url('productos/show/'.$producto['id']) }}">{{ $producto['Categoria'] }} {{ $producto['Grupo'] }} {{ $producto['Color'] }} {{ $producto['Marca'] }}</a></h6>
                    <div class="product_price">
                        <span class="price">${{ number_format($producto['PrecioVenta'],2) }}</span>
                        {{-- <del>$55.25</del>
                        <div class="on_sale">
                            <span>35% Off</span>
                        </div> --}}
                    </div>
                    <div class="rating_wrap">
                        {{-- <div class="rating">
                            <div class="product_rate" style="width:80%"></div>
                        </div> --}}
                        <span class="rating_num">{{ $producto['SKU'] }}</span>
                    </div>
                    <div class="pr_desc">
                        <p>{{ $producto['Descripcion'] }}</p>
                    </div>
                    {{-- <div class="pr_switch_wrap">
                        <div class="product_color_switch">
                            <span class="active" data-color="#87554B"></span>
                            <span data-color="#333333"></span>
                            <span data-color="#DA323F"></span>
                        </div>
                    </div> --}}
                    <div class="list_product_action_box">
                        <ul class="list_none pr_action_btn">
                            <li class="add-to-cart"><a href="{{ url('agregarCarrito/'.$producto['id']) }}"><i class="icon-basket-loaded"></i> Add To Cart</a></li>
                            <li><a href="{{ url('quick_view') }}" class="popup-ajax"><i class="icon-magnifier-add"></i></a></li>
                            @if(Auth::check())
                            <li><a href="#"><i class="icon-heart"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endforeach()
</div>
<div class="row">
    <div class="col-12">
        <ul class="pagination mt-3 justify-content-center pagination_style1">
            @if($productos['last_page'] > 5)
                @if($productos['current_page'] > 3)
                    <li class="page-item"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".($productos['current_page']-1) }}"><i class="linearicons-arrow-left"></i></a></li>
                @endif
                @php
                    if($productos['current_page'] > 2){
                        if(($productos['current_page'] + 2) < $productos['last_page'])
                            $Page1 = $productos['current_page'] - 2;
                        else
                            $Page1 = $productos['last_page'] - 4;
                    }else
                        $Page1 = 1;
                @endphp
                <li class="page-item @if($Page1 == $productos['current_page']) active @endif"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".$Page1 }}">{{ $Page1 }}</a></li>
                @php
                    if($productos['current_page'] > 2){
                        if(($productos['current_page'] + 2) < $productos['last_page'])
                            $Page2 = $productos['current_page'] - 1;
                        else
                            $Page2 = $productos['last_page'] - 3;
                    }else
                        $Page2 = 2;
                @endphp
                <li class="page-item @if($Page2 == $productos['current_page']) active @endif"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".$Page2 }}">{{ $Page2 }}</a></li>
                @php
                    if($productos['current_page'] > 2){
                        if(($productos['current_page'] + 2) < $productos['last_page'])
                            $Page3 = $productos['current_page'];
                        else
                            $Page3 = $productos['last_page'] -2;
                    }else
                        $Page3 = 3;
                @endphp
                <li class="page-item @if($Page3 == $productos['current_page']) active @endif"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".$Page3 }}">{{ $Page3 }}</a></li>
                @php
                    if($productos['current_page'] > 2){
                        if(($productos['current_page'] + 2) < $productos['last_page'])
                            $Page4 = $productos['current_page'] + 1;
                        else
                            $Page4 = $productos['last_page'] -1;
                    }else
                        $Page4 = 4;
                @endphp
                <li class="page-item @if($Page4 == $productos['current_page']) active @endif"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".$Page4 }}">{{ $Page4 }}</a></li>
                @php
                    if($productos['current_page'] > 2){
                        if(($productos['current_page'] + 2) < $productos['last_page'])
                            $Page5 = $productos['current_page'] + 2;
                        else
                            $Page5 = $productos['last_page'];
                    }else
                        $Page5 = 5;
                @endphp
                <li class="page-item @if($Page5 == $productos['current_page']) active @endif"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".$Page5 }}">{{ $Page5 }}</a></li>
                @if(($productos['current_page'] + 2) < $productos['last_page'])
                    <li class="page-item"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".($productos['current_page']+1) }}"><i class="linearicons-arrow-right"></i></a></li>
                @endif
            @else
                @for($i = 1; $i <= $productos['last_page']; $i++)
                    <li class="page-item @if($i == $productos['current_page']) active @endif"><a class="page-link" href="{{ url('productos')."?idf=".Session::get('idf',1)."&idc=".Session::get('idc',0)."&page=".$i }}">{{ $i }}</a></li>
                @endfor()
            @endif
        </ul>
    </div>
</div>
<script>
    $(function(){
        slick_slider();
        carousel_slider();
        $('.popup-ajax').magnificPopup({
            type: 'ajax',
            callbacks: {
                ajaxContentAdded: function() {
                    carousel_slider();
                    slick_slider();
                }
            }
        });
    });
    function carousel_slider() {
		$('.carousel_slider').each( function() {
			var $carousel = $(this);
			$carousel.owlCarousel({
				dots : $carousel.data("dots"),
				loop : $carousel.data("loop"),
				items: $carousel.data("items"),
				margin: $carousel.data("margin"),
				mouseDrag: $carousel.data("mouse-drag"),
				touchDrag: $carousel.data("touch-drag"),
				autoHeight: $carousel.data("autoheight"),
				center: $carousel.data("center"),
				nav: $carousel.data("nav"),
				rewind: $carousel.data("rewind"),
				navText: ['<i class="ion-ios-arrow-left"></i>', '<i class="ion-ios-arrow-right"></i>'],
				autoplay : $carousel.data("autoplay"),
				animateIn : $carousel.data("animate-in"),
				animateOut: $carousel.data("animate-out"),
				autoplayTimeout : $carousel.data("autoplay-timeout"),
				smartSpeed: $carousel.data("smart-speed"),
				responsive: $carousel.data("responsive")
			});
		});
	}
	function slick_slider() {
		$('.slick_slider').each( function() {
			var $slick_carousel = $(this);
			$slick_carousel.slick({
				arrows: $slick_carousel.data("arrows"),
				dots: $slick_carousel.data("dots"),
				infinite: $slick_carousel.data("infinite"),
				centerMode: $slick_carousel.data("center-mode"),
				vertical: $slick_carousel.data("vertical"),
				fade: $slick_carousel.data("fade"),
				cssEase: $slick_carousel.data("css-ease"),
				autoplay: $slick_carousel.data("autoplay"),
				verticalSwiping: $slick_carousel.data("vertical-swiping"),
				autoplaySpeed: $slick_carousel.data("autoplay-speed"),
				speed: $slick_carousel.data("speed"),
				pauseOnHover: $slick_carousel.data("pause-on-hover"),
				draggable: $slick_carousel.data("draggable"),
				slidesToShow: $slick_carousel.data("slides-to-show"),
				slidesToScroll: $slick_carousel.data("slides-to-scroll"),
				asNavFor: $slick_carousel.data("as-nav-for"),
				focusOnSelect: $slick_carousel.data("focus-on-select"),
				responsive: $slick_carousel.data("responsive")
			});
		});
	}
</script>
