<!-- START SECTION CLIENT LOGO -->
<div class="my-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="client_logo carousel_slider owl-carousel owl-theme nav_style1" data-dots="false" data-nav="true" data-margin="30" data-loop="true" data-autoplay="true" data-responsive='{"0":{"items": "2"}, "480":{"items": "3"}, "767":{"items": "4"}, "991":{"items": "5"}}'>
                    @foreach($marcasPrincipales as $marca)
                    <div class="item">
                        <div class="cl_logo">
                            <a href="{{ url('productos/marcas?idm='.$marca['idMarca']) }}"><img src="data:{{ $marca['Mime'] }};base64, {{ $marca['ImagenB64'] }}" alt="{{ $marca['Marca'] }}"/></a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION CLIENT LOGO -->
