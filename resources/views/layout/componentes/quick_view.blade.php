<div class="ajax_quick_view">
	<div class="row">
        <div class="col-lg-6 col-md-6 mb-4 mb-md-0">
          <div class="product-image">
                <div class="product_img_box">
                    @if(!empty($producto->Imagenes))
                        <img id="product_img" src="data:{{ $producto->Imagenes->first()->Foto->mime }};base64, {{ $producto->Imagenes->first()->Foto->thumb }}" data-zoom-image="data:{{ $producto->Imagenes->first()->Foto->mime }};base64, {{ $producto->Imagenes->first()->Foto->zoom }}" alt="product_img1" />
                    @else
                        <img src="{{ asset('images/no_disponible.png') }}" alt="ProductImg">
                    @endif

                </div>
                <div id="pr_item_gallery" class="product_gallery_item slick_slider" data-slides-to-show="4" data-slides-to-scroll="1" data-infinite="false">
                    @if(!empty($producto->Imagenes))
                        @foreach($producto->Imagenes as $imagen)
                        <div class="item">
                            <a href="#" class="product_gallery_item active" data-image="data:{{ $imagen->Foto->mime }};base64, {{ $imagen->Foto->thumb }}" data-zoom-image="data:{{ $imagen->Foto->mime }};base64, {{ $imagen->Foto->zoom }}">
                                <img src="data:{{ $imagen->Foto->mime }};base64, {{ $imagen->Foto->mini }}" alt="product_small_img1" />
                            </a>
                        </div>
                        @endforeach
                    @else
                    <div class="item">
                        <a href="#" class="product_gallery_item active" data-image="{{ asset('images/no_disponible.png') }}" data-zoom-image="{{ asset('images/no_disponible.png') }}">
                            <img src="{{ asset('images/no_disponible.png') }}" alt="product_small_img1" />
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="pr_detail">
                <div class="product_description">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="product_title"><a href="{{ url('productos/show/'.$producto->id) }}">{{ $producto->Categoria }} {{ $producto->Grupo }} {{ $producto->Color }} {{ $producto->Marca }}</a></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="product_price">
                                <span class="price">$45.00</span>
                                {{-- <del>$55.25</del>
                                <div class="on_sale">
                                    <span>35% Off</span>
                                </div> --}}
                            </div>
                            {{-- <div class="rating_wrap">
                                <div class="rating">
                                    <div class="product_rate" style="width:80%"></div>
                                </div>
                                <span class="rating_num">(21)</span>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="pr_desc">
                                <p>{{ $producto->Descripcion }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="product_sort_info mt-1">
                        <ul>
                            <li><i class="linearicons-shield-check"></i> 1 Year AL Jazeera Brand Warranty</li>
                            <li><i class="linearicons-sync"></i> 30 Day Return Policy</li>
                            <li><i class="linearicons-bag-dollar"></i> Cash on Delivery available</li>
                        </ul>
                    </div>
                    {{-- <div class="pr_switch_wrap">
                        <span class="switch_lable">Color</span>
                        <div class="product_color_switch">
                            <span class="active" data-color="#87554B"></span>
                            <span data-color="#333333"></span>
                            <span data-color="#DA323F"></span>
                        </div>
                    </div> --}}
                    <div class="pr_switch_wrap">
                        <span class="switch_lable">Talla</span>
                        <div class="product_size_switch">
                            <span>xs</span>
                            <span>s</span>
                            <span>m</span>
                            <span>l</span>
                            <span>xl</span>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="cart_extra">
                    <div class="cart-product-quantity">
                        <div class="quantity">
                            <input type="button" value="-" class="minus">
                            <input type="text" name="quantity" value="1" title="Qty" class="qty" size="4">
                            <input type="button" value="+" class="plus">
                        </div>
                    </div>
                    <div class="cart_btn">
                        <button class="btn btn-fill-out btn-addtocart" type="button"><i class="icon-basket-loaded"></i> Add to cart</button>
                        {{-- <a class="add_compare" href="#"><i class="icon-shuffle"></i></a> --}}
                        <a class="add_wishlist" href="#"><i class="icon-heart"></i></a>
                    </div>
                </div>
                <hr />
                <ul class="product-meta">
                    <li>SKU: <a href="#">{{ $producto->SKU }}</a></li>
                    <li>Familia: <a href="#">{{ $producto->Familia }}</a></li>
                    <li>Categoria: <a href="#">{{ $producto->Categoria }}</a></li>
                    <li>Marca: <a href="#">{{ $producto->Marca }}</a></li>
                    <li>Grupo: <a href="#">{{ $producto->Grupo }}</a></li>
                    <li>Color: <a href="#">{{ $producto->Color }}</a></li>
                    {{-- <li>Tags: <a href="#" rel="tag">Cloth</a>, <a href="#" rel="tag">printed</a> </li> --}}
                </ul>

                {{-- <div class="product_share">
                    <span>Share:</span>
                    <ul class="social_icons">
                        <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                        <li><a href="#"><i class="ion-social-youtube-outline"></i></a></li>
                        <li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
                    </ul>
                </div> --}}
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/scripts.js') }}"></script>
