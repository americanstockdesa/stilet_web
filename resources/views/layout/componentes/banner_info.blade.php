<div class="section small_pt small_pb">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <table width="100%">
                    <tr>
                        <td align="center"><img src="{{ asset('images/zapato.png') }}" /></td>
                    </tr>
                    <tr>
                        <td align="center"><h5 class="footBanner">Calzado Garantizado</h5></td>
                    </tr>
                    <tr>
                        <td align="center" class="footBanner">Cuidamos la calidad de nuestros productos garantizamos <span class="footBannerResaltado">comodidad, resistencia y durabilidad</span></td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-3">
                <table width="100%">
                    <tr>
                        <td align="center"><img src="{{ asset('images/camion.png') }}" /></td>
                    </tr>
                    <tr>
                        <td align="center"><h5 class="footBanner">Calzado Garantizado</h5></td>
                    </tr>
                    <tr>
                        <td align="center" class="footBanner">Cuidamos la calidad de nuestros productos garantizamos <span class="footBannerResaltado">comodidad, resistencia y durabilidad.</span></td>
                    </tr>
                </table>
            </div>

            <div class="col-lg-3">
                <table width="100%">
                    <tr>
                        <td align="center"><img src="{{ asset('images/zapato_etiqueta.png') }}" /></td>
                    </tr>
                    <tr>
                        <td align="center"><h5 class="footBanner">Ofertas y Promociones</h5></td>
                    </tr>
                    <tr>
                    <td align="center" class="footBanner">Consulta nuestras promociones <a href="{{ url('productos/promociones') }}"><span class="footBannerResaltado">aquí,</span></a> los mejores pares al mejor precio.</td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-3">
                <table width="100%">
                    <tr>
                        <td align="center"><img src="{{ asset('images/tarjeta.png') }}" /></td>
                    </tr>
                    <tr>
                        <td align="center"><h5 class="footBanner">Pago Seguro</h5></td>
                    </tr>
                    <tr>
                        <td align="center" class="footBanner"><span class="footBannerResaltado">Protegemos </span>tus datos personales e información de pago.</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
