<style>
    .bgTop {
    background-color: #384354 !important;
}
.top-header {
    border-bottom: 0px solid #eee;
}
.headLink {
    color: #fff;
}
.headLink:hover {
    color: #c55844;
}
.nopad {
    padding: 10px !important;
}
.footNew {
    background-color:#FDF7D2 !important;
    padding: 20px !important;
}
.headLetterNew {
    color: #c55844 !important;
}
.footLetterNew {
    color: #505050 !important;
}
.footLetterNewLink:hover {
    color: #c55844 !important;
}
.SubFootNew {
    background-color: white !important;
    padding: 0px !important;
}
.search_btn {
    background-color: #f0bf60 !important;
}
.categories_btn {
    background-color: #c45744 !important;
    border: 1px solid #c45744 !important;
}
.product_search_form {
    -webkit-box-shadow: 4px 4px 6px 0px rgba(68, 68, 68, 0.753);
    -moz-box-shadow: 4px 4px 6px 0px rgba(68, 68, 68, 0.753);
    box-shadow: 4px 4px 6px 0pxrgba(68, 68, 68, 0.753);
}
.headAmarillo {
    background-color: #f0bf60 !important;
}

.footBanner {
    color: #364456;
}
.footBannerResaltado {
    color: #d5513e;
}
.dropdown-menu {
    min-width: 400px !important;
}
.iconSVG  {
    display: inline-block;
    width: 18px;
    height: 18px;
    background-size: cover;
    margin-left: 2px;
    margin-right: 12px;
}
.iconSVG-girl {
    background-image: url({{ asset('images/icons/birthday-girl.svg') }});
    filter: brightness(150%) contrast(37%);
}
.iconSVG-boy {
    margin-top: 2px;
    background-image: url({{ asset('images/icons/estudiante.svg') }});
    filter: brightness(150%) contrast(87%);
}
.iconSVG-baby {
    background-image: url({{ asset('images/icons/baby-boy.svg') }});
    filter: brightness(150%) contrast(87%);
}
.iconSVG-bag {
    background-image: url({{ asset('images/icons/handbag.svg') }});
    filter: brightness(150%) contrast(87%);
}
.LinkActivo {
    color: #dc3545 !important;
}
</style>
