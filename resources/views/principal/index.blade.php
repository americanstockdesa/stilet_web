@extends('layout.app')
@section('css')
    @include('layout.css.principal')
@endsection

@section('body')

    @include('layout.componentes.home_popup')
    
    @include('layout.header',compact('categorias'))

    @include('layout.componentes.banner_principal',compact('bannersPrincipales'))

    @include('layout.componentes.our_brands',compact('marcasPrincipales'))
    <!-- END MAIN CONTENT -->
    <div class="main_content">

        @include('layout.componentes.nuevas_colecciones',compact('nuevasColecciones'))

        @include('layout.componentes.promociones',compact('promociones'))

        @include('layout.componentes.ultimos_pares',compact('ultimosPares'))

        @include('layout.componentes.banner_info')
    </div>
    <!-- END MAIN CONTENT -->

    @include('layout.footer')
@endsection
