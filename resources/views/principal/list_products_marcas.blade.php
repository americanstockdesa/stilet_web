@extends('layout.app')

@section('css')
@include('layout.css.productos')
@endsection

@section('body')
    @include('layout.header_products',compact('categorias'))
    @include('layout.componentes.breadcrumb')


<!-- START MAIN CONTENT -->
<div class="main_content">

    <!-- START SECTION SHOP -->
    <div class="section pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="row align-items-center mb-4 pb-1">
                        <div class="col-12">
                            <div class="product_header">
                                <div class="product_header_left">
                                    <div class="custom_select">
                                        <select class="form-control form-control-sm">
                                            <option value="order">Orden por Defecto</option>
                                            <option value="popularity">Ordenar por Popularidad</option>
                                            <option value="date">Ordenar por los Mas Nuevos</option>
                                            <option value="price">Ordenar Por Precio: Bajo a Alto</option>
                                            <option value="price-desc">Ordenar Por Precio: Alto a Bajo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="product_header_right">
                                    <div class="products_view">
                                        <a href="javascript:Void(0);" class="shorting_icon grid active"><i class="ti-view-grid"></i></a>
                                        <a href="javascript:Void(0);" class="shorting_icon list"><i class="ti-layout-list-thumb"></i></a>
                                    </div>
                                    {{-- <div class="custom_select">
                                        <select class="form-control form-control-sm">
                                            <option value="">Showing</option>
                                            <option value="9">9</option>
                                            <option value="12">12</option>
                                            <option value="18">18</option>
                                        </select>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="shop_container">

                    </div>
                </div>
                <div class="col-lg-3 order-lg-first mt-4 pt-2 mt-lg-0 pt-lg-0">
                    <div class="sidebar">
                        <div class="widget">
                            <h5 class="widget_title">Marcas Principales</h5>
                            <ul class="widget_categories">
                                @foreach($productos['Marcas'] as $marca)
                                    <li><a @if($marca['idMarca'] == Session::get('idm',0)) class="LinkActivo" @endif href="{{ url('productos/marcas')."?idm={$marca['idMarca']}" }}"><span class="categories_name">{{ $marca['Marca'] }}</span><span class="categories_num">({{ $marca['Cantidad'] }})</span></a></li>
                                @endforeach()
                            </ul>
                        </div>
                        <div class="widget">
                            <h5 class="widget_title">Categorias</h5>
                            <ul class="widget_categories">
                                @foreach($productos['Categorias'] as $categoria)
                                    <li><a @if(Session::get('idc',0) > 0 && Session::get('idc',0) == $categoria['idCategoria']) class="LinkActivo" @endif href="{{ url('productos/marcas')."?idm=".Session::get('idm',0)."&idc={$categoria['idCategoria']}" }}"><span class="categories_name">{{ $categoria['Categoria'] }}</span><span class="categories_num">({{ $categoria['Cantidad'] }})</span></a></li>
                                @endforeach()
                            </ul>
                        </div>
                        {{-- <div class="widget">
                            <h5 class="widget_title">Filtro de Precio</h5>
                            <div class="filter_price">
                                <div id="price_filter" data-min="0" data-max="500" data-min-value="50" data-max-value="300" data-price-sign="$"></div>
                                <div class="price_range">
                                    <span>Precio: <span id="flt_price"></span></span>
                                    <input type="hidden" id="price_first">
                                    <input type="hidden" id="price_second">
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="widget">
                            <h5 class="widget_title">Marcas</h5>
                            <ul class="list_brand">
                                @foreach($productos['Marcas'] as $marca)
                                <li>
                                    <div class="custome-checkbox">
                                        <input class="form-check-input" type="checkbox" name="checkbox" id="{{ $marca['Marca'] }}" value="">
                                        <label class="form-check-label" for="Arrivals"><span>{{ $marca['Marca'] }} ({{ $marca['Cantidad'] }})</span></label>
                                    </div>
                                </li>
                                @endforeach()
                            </ul>
                        </div> --}}
                        <div class="widget">
                            <h5 class="widget_title">Tallas</h5>
                            <div class="product_size_switch">
                                @foreach($productos['Tallas'] as $talla)
                                    <a @if(Session::get('idt',0) > 0 && Session::get('idt',0) == $talla) class="LinkActivo" @endif href="{{ url('productos/marcas')."?idm=".Session::get('idm',0).'&idc='.Session::get('idc',0).'&idt='.$talla }}"><span>{{ $talla }}</span></a>
                                @endforeach
                            </div>
                        </div>
                        {{-- <div class="widget">
                            <h5 class="widget_title">Color</h5>
                            <div class="product_color_switch">
                                <span data-color="#87554B"></span>
                                <span data-color="#333333"></span>
                                <span data-color="#DA323F"></span>
                                <span data-color="#2F366C"></span>
                                <span data-color="#B5B6BB"></span>
                                <span data-color="#B9C2DF"></span>
                                <span data-color="#5FB7D4"></span>
                                <span data-color="#2F366C"></span>
                            </div>
                        </div> --}}
                        {{-- <div class="widget">
                            <div class="shop_banner">
                                <div class="banner_img overlay_bg_20">
                                    <img src="{{ asset("images/sidebar_banner_img.jpg") }}" alt="sidebar_banner_img">
                                </div>
                                <div class="shop_bn_content2 text_white">
                                    <h5 class="text-uppercase shop_subtitle">New Collection</h5>
                                    <h3 class="text-uppercase shop_title">Sale 30% Off</h3>
                                    <a href="#" class="btn btn-white rounded-0 btn-sm text-uppercase">Shop Now</a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SECTION SHOP -->

</div>
<!-- END MAIN CONTENT -->

@include('layout.footer')

@endsection

@section('script')
<script>
    $(function(){
        $('#shop_container').load('{{ url("productos/get_productos_marca") }}');
    });
</script>
@endsection
